import math

xA = int(input("Coordenada X: "))
yA = int(input("Coordenada Y: "))

xB = int(input("Digite outro ponto X do mesmo plano: "))
yB = int(input("Digite outro ponto Y do mesmo plano: "))

pontos = math.sqrt(((xB-xA)**2)+ ((yB-yA)**2))

if(pontos >= 10):
    print("longe")
else:
    print("perto")
